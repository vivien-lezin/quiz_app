import 'package:adv_basics/model/question.dart';
import 'package:adv_basics/model/response.dart';

List<Question> questions = [
  Question("What are the main building blocks of flutter UIs?", 0, [
    Response("Widgets"),
    Response("Components"),
    Response("Blocks"),
    Response("Functions"),
  ]),
  Question("What's the purpose of a StatefulWidget?", 2, [
    Response("Update data as UI changes"),
    Response("Ignore data changes"),
    Response("Update UI as data changes"),
    Response("Render UI that doesn't changed on data"),
  ]),
  Question("How are Flutter UIs built?", 0, [
    Response("By using XCode for iOS and Android Studio for Android"),
    Response("By combining widgets in code"),
    Response("By combining widget in virtual machine"),
    Response("By changing widget in coding filters"),
  ]),
  Question(
      "Which widget should you try to use more often: StatelessWidget or StatefulWidget?",
      0, [
    Response("StatelessWidget"),
    Response("StatefulWidget"),
    Response("Both are equally good"),
    Response("None of the above"),
  ]),
  Question("What happens if you change data in a StatelessWidget?", 2, [
    Response("The UI is updated"),
    Response("The closest StatefulWidget is updated"),
    Response("The UI is not updated"),
    Response("Any nested StatefulWidgets are updated"),
  ]),
  Question(
    'How should you update data inside of StatefulWidgets?',
    3,
    [
      Response('By calling updateData()'),
      Response('By calling updateUI()'),
      Response('By calling updateState()'),
      Response('By calling setState()'),
    ],
  ),
];
