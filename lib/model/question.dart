import 'package:adv_basics/model/response.dart';

class Question {
  String label;
  int indexGoodResponse;
  List<Response> responses;
  late int currentIndexResponse;

  Question(this.label, this.indexGoodResponse, this.responses);
}
