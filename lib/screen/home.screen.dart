import 'package:flutter/material.dart';

class Home extends StatelessWidget {

  final void Function() onPressedButton;

  const Home(this.onPressedButton, {super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            constraints: const BoxConstraints(
              maxWidth: 250,
            ),
            child: Image.asset("assets/images/quiz-logo.png",
                color: const Color.fromARGB(153, 255, 255, 255)),
          ),
          const SizedBox(
            height: 50,
          ),
          const Text(
            "Learn Flutter the fun way!",
            style: TextStyle(fontSize: 24, color: Colors.white),
          ),
          const SizedBox(
            height: 20,
          ),
          OutlinedButton.icon(
              icon: const Icon(
                Icons.arrow_forward_ios_outlined,
              ),
              style: OutlinedButton.styleFrom(foregroundColor: Colors.white),
              onPressed: onPressedButton,
              label: const Text(
                "Start quizz",
              ))
        ],
      ),
    );
  }
}
