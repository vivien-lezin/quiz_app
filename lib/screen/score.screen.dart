import 'package:adv_basics/model/data/questions-data.dart';
import 'package:adv_basics/model/question.dart';
import 'package:adv_basics/widget/result.widget.dart';
import 'package:flutter/material.dart';

class ScoreScreen extends StatelessWidget {
  final List<Question> chosenAnswers;
  final void Function() reset;

  const ScoreScreen({super.key, required this.chosenAnswers, required this.reset});

  @override
  Widget build(BuildContext context) {
    int currentScore = 0;
    for (var question in chosenAnswers) {
      if (question.currentIndexResponse == question.indexGoodResponse){
        currentScore++;
      }
    }
    return Center(
      child: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              "Well done!",
              style: TextStyle(color: Colors.white, fontSize: 24),
              textAlign: TextAlign.center,
            ),
            Text("$currentScore/${questions.length}",
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 36,
                    fontWeight: FontWeight.bold)),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              height: 300,
              width: double.infinity,
              child: ListView.builder(
                itemCount: questions.length,
                itemBuilder: (context, index) {
                  return Result(question: questions[index],index: index,);
                },
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            TextButton.icon(
              icon: const Icon(
                Icons.restart_alt_rounded,
                color: Colors.white,
              ),
                onPressed: reset,
                label: const Text(
                  "Restart",
                  style: TextStyle(color: Colors.white),
                ))
          ],
        ),
      ),
    );
  }
}
