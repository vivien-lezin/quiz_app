import 'package:adv_basics/model/data/questions-data.dart';
import 'package:adv_basics/model/question.dart';
import 'package:adv_basics/widget/question.widget.dart';
import 'package:flutter/material.dart';

class QuestionsScreen extends StatefulWidget {
  final void Function(List<Question>) questionaryAnswered;

  const QuestionsScreen({super.key, required this.questionaryAnswered});

  @override
  State<QuestionsScreen> createState() =>
      _QuestionsScreenState();
}


class _QuestionsScreenState extends State<QuestionsScreen> {
  int currentIndex = 0;

  _QuestionsScreenState();

  void onTape(int index) {
    questions[currentIndex].currentIndexResponse = index;
    setState(() {
      if (questions.length > currentIndex + 1) {
        currentIndex++;
      } else {
        //Screen score
        widget.questionaryAnswered(questions);
      }

    });
  }

  @override
  Widget build(BuildContext context) {
    return QuestionWidget(
      question: questions[currentIndex],
      onResultTap: onTape,
    );
  }
}
