import 'package:adv_basics/model/question.dart';
import 'package:adv_basics/model/response.dart';
import 'package:adv_basics/screen/home.screen.dart';
import 'package:adv_basics/screen/questions.screen.dart';
import 'package:adv_basics/screen/score.screen.dart';
import 'package:flutter/material.dart';

class Quiz extends StatefulWidget {
  const Quiz({super.key});

  @override
  State<Quiz> createState() => _QuizState();
}

class _QuizState extends State<Quiz> {
  Widget? activeScreen;

  late void Function(List<Question>) questionaryAnswered = (List<Question> questions) {
    setState(() {
      activeScreen = ScoreScreen(chosenAnswers: questions,reset: switchScreen,);
    });
  };

  @override
  void initState() {
    activeScreen = Home(switchScreen);
    super.initState();
  }

  void switchScreen() {
    setState(() {
      activeScreen = QuestionsScreen(
        questionaryAnswered: questionaryAnswered,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(useMaterial3: true),
        home: Scaffold(
            body: Container(
              decoration: const BoxDecoration(
                  gradient: LinearGradient(colors: [
                Color.fromRGBO(27, 66, 93, 1.0),
                Color.fromRGBO(41, 143, 152, 1.0),
              ], begin: Alignment.topLeft, end: Alignment.bottomRight)),
              child: activeScreen,
            )));
  }
}
