import 'package:adv_basics/model/question.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class QuestionWidget extends StatelessWidget {
  final Question question;
  final void Function(int) onResultTap;

  const QuestionWidget(
      {super.key, required this.question, required this.onResultTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(question.label,
              style: GoogleFonts.lato(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: question.responses.length,
                itemBuilder: (contextItemBuilder, index) {
                  return Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: ElevatedButton(
                      style: const ButtonStyle(
                        foregroundColor: MaterialStatePropertyAll(Colors.white),
                        backgroundColor: MaterialStatePropertyAll(Color.fromRGBO(
                            10, 70, 70, 1.0)),
                      ),
                      onPressed: () {
                        String message;
                        if (question.indexGoodResponse == index) {
                          message = "Well done!";
                        } else {
                          message = "Nope!";
                        }
                        onResultTap(index);
                        ScaffoldMessenger.of(context)
                            .showSnackBar(SnackBar(content: Text(message)));
                      },
                      child: ConstrainedBox(
                          constraints: const BoxConstraints(maxHeight: 100),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16, vertical: 8),
                            child: Text(question.responses[index].label),
                          )),
                    ),
                  );
                },
              )),
        ],
      ),
    );
  }
}
