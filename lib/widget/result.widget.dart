import 'package:adv_basics/model/question.dart';
import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final Question question;
  final int index;

  const Result({super.key, required this.question, required this.index});

  @override
  Widget build(BuildContext context) {
    Color boxColor = Colors.red;
    if (question.indexGoodResponse == question.currentIndexResponse){
      boxColor = Colors.green;
    }
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
      child: DecoratedBox(
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: Color.fromRGBO(0, 0, 0, 0.1)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: DecoratedBox(
                  decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(50)),
                      color: boxColor),
                  child: SizedBox(
                    height: 36,
                    width: 36,
                    child: Center(
                      child: Text(
                        "${index+1}",
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  )),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0,8,8,8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      question.label,
                      textAlign: TextAlign.start,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      question.responses[question.currentIndexResponse].label,
                      textAlign: TextAlign.start,
                      style: const TextStyle(color: Colors.red),
                    ),
                    Text(
                      question.responses[question.indexGoodResponse].label,
                      textAlign: TextAlign.start,
                      style: const TextStyle(color: Colors.green),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
